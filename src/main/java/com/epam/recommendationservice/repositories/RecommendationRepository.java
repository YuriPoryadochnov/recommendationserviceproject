package com.epam.recommendationservice.repositories;

import com.epam.recommendationservice.models.Recommendation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecommendationRepository extends JpaRepository<Recommendation, Long> {
}
