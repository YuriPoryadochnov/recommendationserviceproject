package com.epam.recommendationservice.controllers;

import com.epam.recommendationservice.models.Recommendation;
import com.epam.recommendationservice.repositories.RecommendationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class RecommendationController {
    @Autowired
    RecommendationRepository recommendationRepository;

    @RequestMapping(value = "/recommendation/{userEmail}/{id}", method = RequestMethod.GET, produces = "application/json")
    public List<Long> getRecommendations(@PathVariable("userEmail") String userEmail, @PathVariable("id") Long id) {
        List<Recommendation> recommendations = recommendationRepository.findAll();
        List<Long> recList = new ArrayList<>();
        for (Recommendation recommendation : recommendations) {
            if (recommendation.getProductId() == id && recommendation.getUserEmail().equals(userEmail)) {
                recList.add(recommendation.getProductRecommendationId());
            }
        }
        return recList;
    }
}
