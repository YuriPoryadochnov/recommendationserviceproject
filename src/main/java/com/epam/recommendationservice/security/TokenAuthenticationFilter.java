package com.epam.recommendationservice.security;

import com.epam.recommendationservice.logging.LogRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class TokenAuthenticationFilter extends GenericFilterBean {
    @Value("${url.auth-service}")
    private String AUTH_URL;

    @Value("${jwt.header}")
    private String AUTH_HEADER;

    @Autowired
    TokenHelper tokenHelper;

    private Logger logger = LoggerFactory.getLogger("service");

    private String getToken(String string) {
        if (string != null && string.startsWith("Bearer ")) {
            return string.substring(7);
        }
        return null;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        LogRequest logRequest = new LogRequest((HttpServletRequest) request);
        final String authHeader = ((HttpServletRequest) request).getHeader(AUTH_HEADER);
        final String userEmail = tokenHelper.getUsernameFromToken(getToken(authHeader));

        if (authHeader != null && userEmail != null) {

            HttpHeaders headers = new HttpHeaders();
            headers.set(AUTH_HEADER, authHeader);

            ResponseEntity<HttpStatus> authResponse = new RestTemplate().exchange(
                    AUTH_URL,
                    HttpMethod.GET,
                    new HttpEntity<>(headers),
                    HttpStatus.class);

            if (authResponse.getStatusCode() == HttpStatus.OK) chain.doFilter(request, response);
            logger.debug("Recommendation-service: " + logRequest.toString());
        }
    }
}

