package com.epam.recommendationservice.models;

import javax.persistence.*;

@Entity
@Table(name = "recommendations")
public class Recommendation {
    private long id;
    private long productId;
    private String userEmail;
    private long productRecommendationId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public long getProductRecommendationId() {
        return productRecommendationId;
    }

    public void setProductRecommendationId(long productRecommendationId) {
        this.productRecommendationId = productRecommendationId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
}
