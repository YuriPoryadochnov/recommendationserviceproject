package com.epam.recommendationservice.logging;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class LogRequest {
    @JsonIgnore
    private long timeOfRequest;

    private long timeOfProcessingRequest;
    private Map<String, String> headers;
    private String body;

    public LogRequest(){}

    public LogRequest(HttpServletRequest httpServletRequest) {
        this.timeOfRequest = System.currentTimeMillis();
        this.headers = getHeadersFromRequest(httpServletRequest);
        this.body = getBodyFromRequest(httpServletRequest);
    }

    private Map<String, String> getHeadersFromRequest(HttpServletRequest httpServletRequest){
        Map<String, String> headers = new HashMap<>();
        Enumeration<String> requestHeaders = httpServletRequest.getHeaderNames();
        for (String requestHeader = requestHeaders.nextElement(); requestHeaders.hasMoreElements(); requestHeader = requestHeaders.nextElement()){
            headers.put(requestHeader, httpServletRequest.getHeader(requestHeader));
        }
        return headers;
    }

    private String getBodyFromRequest(HttpServletRequest httpServletRequest){
        try{
            return httpServletRequest.getReader().lines().collect(Collectors.joining());
        } catch (IOException e) {
            return null;
        }
    }

    public long getTimeOfRequest() {
        return timeOfRequest;
    }

    public void setTimeOfRequest(long timeOfRequest) {
        this.timeOfRequest = timeOfRequest;
    }

    public long getTimeOfProcessingRequest() {
        return timeOfProcessingRequest;
    }

    public void setTimeOfProcessingRequest(long timeOfProcessingRequest) {
        this.timeOfProcessingRequest = timeOfProcessingRequest;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString(){
        this.timeOfProcessingRequest = System.currentTimeMillis() - timeOfRequest;
        try(StringWriter writer = new StringWriter()) {
            new ObjectMapper().writeValue(writer, this);
            return writer.toString();
        } catch (IOException e) {
            return super.toString();
        }
    }
}
